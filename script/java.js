//1._Cree un script que solicite un valor numérico al usuario en base octal (8) y posteriormente 
//muestre su equivalente en base decimal (10). Muestre el resultado en una ventana emergente.

function ejercicio1(){
    let numOctal = prompt("Ingrese un Número en base Octal(8):");

    if (numOctal == ""){
        alert("Campo vacio, Ingrese un número");

    }else{
        let numero = parseInt(numOctal, 8);
        alert("El número en base decimal es:" + parseInt(numero, 10));
    }

}

//2.Cree un script que defina un objeto llamado Producto_alimenticio, este objeto debe presentar 
//las propiedades código, nombre y precio, además del método imprimeDatos, el cual escribe por 
//pantalla los valores de sus propiedades. Posteriormente, cree tres instancias de este objeto y 
//guárdelas en un array. Con la ayuda del bucle for, utilice el método imprimeDatos para mostrar por 
//pantalla los valores de los tres objetos instanciados.

function Producto_alimenticio(codigo, nombre, precio){
    this.codigo = codigo;
    this.nombre = nombre;
    this.precio = precio;
}

var productoArray = new Array();
productoArray[0] = new Producto_alimenticio(001,"CocaCola", 0.75);
productoArray[1] = new Producto_alimenticio(002, "leche", 0.60);
productoArray[2] = new Producto_alimenticio(003, "queso", 2.00);

function imprimirDatos(){
    productoArray.forEach(producto =>{
        document.write(`<h1 style="text-align:center;background-color:#17B890;color:white;font-size:25px;font-weight:bolder;margin-top:40px">Registro ${productoArray.indexOf(producto)}<h1>`);
        document.write(`<p style="text-align:center;font-size:20px"><strong>Código:</strong> ${producto.codigo}</p>`);
        document.write(`<p style="text-align:center; font-size:20px"><strong>Nombre:</strong> ${producto.nombre}</p>`);
        document.write(`<p style="text-align:center;font-size:20px"><strong>Precio:$</strong> ${producto.precio}</p>`);

    });
}

